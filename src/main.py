import argparse
from presigned_url import create_presigned_url


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Signed URL test")
    parser.add_argument("--bucket", type=str, help="Bucket name")
    parser.add_argument("--object", type=str, help="object name")
    args = parser.parse_args()
    url = create_presigned_url(args.bucket, args.object)
    if url is not None:
        print(url)

